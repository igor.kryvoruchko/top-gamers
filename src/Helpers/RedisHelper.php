<?php


namespace App\Helpers;


use Predis\Client;

class RedisHelper
{
    protected $redis;

    /**
     * RedisHelper constructor.
     * @param $redis
     */
    public function __construct()
    {
        $this->redis = new Client([
            'host' => '127.0.0.1',
            'port' => 6379,
        ]);
    }

    public function set($name, $score)
    {
        $this->redis->zadd('gamers', [$name => $score]);
    }

    public function getTop()
    {
        return $this->redis->zrange('gamers', -10, -1, 'WITHSCORES');
    }

    public function delete($name)
    {
        return $this->redis->zrem('gamers', $name);
    }


}