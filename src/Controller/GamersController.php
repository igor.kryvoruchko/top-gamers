<?php

namespace App\Controller;

use App\Helpers\RedisHelper;
use http\Client\Response;
use Symfony\Component\HttpFoundation\Request;
use Predis\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GamersController extends AbstractController
{
    /**
     * @Route("/", name="gamers")
     */
    public function index()
    {
        $redis = new RedisHelper();
        $topGamers = $redis->getTop();
        arsort($topGamers);

        return $this->render('gamers/index.html.twig', [
            'topGamers' => $topGamers
        ]);
    }

    /**
     * @Route("/add", name="add", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function add(Request $request)
    {
        if($request->get('name') && $request->get('score')){
            $redis = new RedisHelper();
            $redis->set($request->get('name'), $request->get('score'));
        }

        return $this->redirect('/');
    }

    /**
     * @Route("/delete", name="delete", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request)
    {
        if($request->get('name')){
            $redis = new RedisHelper();
            if($redis->delete($request->get('name'))){
                return $this->json([
                    'status' => true,
                    'message' => $request->get('name')." Deleting"
                ]);
            }
        }
        return $this->json([
            'status' => false
        ]);
    }
}
